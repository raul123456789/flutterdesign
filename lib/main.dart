import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FlutterDesign',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Dashboard'),
    );
  }
}

class MyHomePage extends StatefulWidget {

  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {

  TabController controllerTab;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controllerTab = new TabController(length: 4,vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
            child: Icon(Icons.access_alarms))
        ],
      ),
      drawer: getNavDrawer(context),
      bottomNavigationBar: getTabBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
              width: double.infinity,
              height: 250,
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(20),
                color: Colors.blueAccent
              ),
              child:
              ClipOval(
                child: Container (
                  height: 95,
                  child:
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 50),
                            child: ClipOval(
                              child: Image.asset(
                                    "assets/emp.jpg",
                                height: 75,
                                width: 70,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 12,bottom: 40),
                            child: Column(
                              children: <Widget>[
                                 Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 0),
                                child: Text("Gowtham Raj",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                              ),
                              Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 0),
                                    child: Text("UI/UX Designer",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
          ),
          Container(
            width: double.infinity,
            height: 320,
            decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(30),
                color: Colors.black12
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("Recommended Jobs",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 120,top: 8),
                        child: Text("See all",style: TextStyle(color: Colors.purple,fontWeight: FontWeight.bold),),
                      ),
                    ],
                  ),

                  Container(
                    padding: EdgeInsets.only(left: 10),
                    width: 345,
                    height: 220,
                    child: Card(
                        child: Row(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ClipOval(
                                  child: Image.asset(
                                    "assets/hp.png",
                                    height: 45,
                                    width: 40,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(top: 5),
                                      child: Text("   UX Product Engineer ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(right: 55),
                                      child: Text("Zoho Corp",style: TextStyle(color: Colors.black38,fontWeight: FontWeight.bold),),
                                    ),
                                    Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(top: 10,right: 10,left: 2),
                                          child: Container(
                                            width: 150,
                                            height: 90,
                                            decoration: new BoxDecoration(
                                                borderRadius: new BorderRadius.circular(10),
                                                color: Colors.black26
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Column(
                                                children: <Widget>[
                                                  Text("Chennai",style: TextStyle(color: Colors.black38,fontWeight: FontWeight.bold),),
                                                  Spacer(),
                                                  Text("Photoshop, Adobe XD,Invision studio",
                                                    style: TextStyle(color: Colors.black38,fontWeight: FontWeight.bold),),

                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: <Widget>[
                                            Container(
                                                width: 90,
                                                height: 40,
                                                decoration: new BoxDecoration(
                                                    borderRadius: new BorderRadius.circular(10),
                                                    color: Colors.lightGreenAccent
                                                ),
                                                child: Center(child: Text("Accept" ,style: TextStyle(color: Colors.green,fontWeight: FontWeight.bold),))
                                            ),
                                          Padding(
                                            padding: EdgeInsets.all(5),
                                          ),
                                          Container(
                                              width: 90,
                                              height: 40,
                                              decoration: new BoxDecoration(
                                                  borderRadius: new BorderRadius.circular(10),
                                                  color: Colors.orange[100]
                                              ),
                                              child: Center(child: Text("Reject" ,style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold),))
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[

                              ],
                            )
                          ],
                        ),
                    ),
                  )

                ],
            ),
          )
        ],
      ),
    );
  }

  Drawer getNavDrawer(BuildContext context) {

    var headerChild = new GestureDetector(
        onTap: () {

        },
        child: MediaQuery.removePadding(
          context: context,
          // DrawerHeader consumes top MediaQuery padding
          removeTop: true,
          child:
          Container (

          ),
        ));

    ListTile getNavItem(String s, String routeName,{var icon, var iconBtn}) {
      return new ListTile(
        leading: (icon != null)
            ? new Icon(icon)
            : IconButton(icon: Image.asset(iconBtn)),
        title: new Text(s),
        onTap: () {
          setState(() {
            // pop closes the drawer
            Navigator.of(context).pop();
            // navigate to the route
//            Navigator.of(context).pushNamed(routeName);
            switch (routeName) {
              case "news":
               /* Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CommonModulePage(
                            "News",
                            network.fetchPost("news",
                                moduleId: moduleInfo["3"]["id"]),
                            moduleInfo["3"]["id"]
                        )));*/
                break;

              case "files":
               /* Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CommonModulePage(
                            "Resources",
                            network.fetchPost(VALUE_MODULE_SHARE_FILES,
                                moduleId: moduleInfo["4"]["id"]),
                            moduleInfo["4"]["id"])));*/
                break;

              case "IT Act":
             /*   Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ITAct(getSections(id))));*/
                break;

            /*case "e_learn":
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OnlineLearning()));
                break;*/
              case "bookmarks":
            /*    Navigator.push(context,
                    MaterialPageRoute(builder: (context) => BookmarkPage()));*/
                break;

              case "icds":
              /* Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LeaderBoard()));*/
                break;
              case "leaderboard":
               /* Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Leaderboard()));*/
                break;
            /* case "remove":
              *//* Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LeaderBoard()));*//*
                break;*/
              case "settings":
                /*Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Settings()));*/
                break;
              case "shareapp":
               /* Share.share(
                    "Download Income Tax Act Android App"+
                        "\n http://play.google.com/store/apps/details?id="
                        +"com.offlineappsindia.incometaxacts"
                );*/
                break;
              case "otherapps":
               // launchURL();
                break;
              case "taxationlaws":
              /*Navigator.push(context,
                      MaterialPageRoute(builder: (context) => BookmarkPage()));*/
                break;
            }
          });
        },
      );
    }

    var myNavChildren = [
      headerChild,
      getNavItem(
        "News",
        "news",
        icon: Icons.person,
      ),
      getNavItem(
        "Files",
        "files",
        icon: Icons.attach_file,
      ),
      getNavItem(
        "IT Act 2018",
        "IT Act",
        icon: Icons.book,
      ),
      getNavItem(
        "Bookmarks/Notes",
        "bookmarks",
        icon: Icons.star,

      ),
      getNavItem(
        "Leaderboard",
        "leaderboard",
        icon: Icons.insert_chart,
      ),
      getNavItem(
        "Settings",
        "settings",
        icon: Icons.settings,
      ),
      getNavItem(
          "Share App",
          "shareapp",
          icon: Icons.share
      ),
      getNavItem(
          "Other Apps",
          "otherapps",
          icon: Icons.chrome_reader_mode
      ),
      getNavItem(
          "Taxation laws (Ordinance)",
          "taxationlaws",
          icon: Icons.subject
      ),
    ];
    ListView listView = new ListView(children: myNavChildren);
    return new Drawer(
      child: Padding(
        padding: EdgeInsets.all(2),
        child: listView,
      ),
    );
  }

  Widget getTabBar() {
      return BottomBarWidget(controllerTab);
  }
}

class BottomBarWidget extends StatefulWidget{

  TabController tabController;


  BottomBarWidget(this.tabController);

  @override
  State<StatefulWidget> createState() {

    return BottomBarState(tabController);
  }

}

class BottomBarState extends State<BottomBarWidget>{

  TabController controllerTab;
  BottomBarState(this.controllerTab);

  @override
  void initState() {

    controllerTab.addListener((){
      setState(() {

      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
        elevation: 8,
        child: new TabBar(
          labelColor: Colors.black,
          labelStyle: TextStyle(fontSize: 12),
          tabs: <Tab>[
            new Tab(
              // set icon to the tab
              text: "Home",
              icon: new Icon(
                Icons.content_paste,
                color: (controllerTab.index==0)?Colors.black:Colors.black54,
              ),
            ),
            new Tab(
              text: "LOG",
              icon: new Icon(
                Icons.description,
                color: (controllerTab.index==1)?Colors.black:Colors.black54,
              ),
            ),
            new Tab(
              text: "My REVIEW",
              icon: new Icon(
                Icons.thumb_up,
                color: (controllerTab.index==2)?Colors.black:Colors.black54,
              ),
            ),
            new Tab(
              text: "INVOICES",
              icon: new Icon(
                Icons.note,
                color: (controllerTab.index==3)?Colors.black:Colors.black54,
              ),
            ),
          ],
          // setup the controller
          controller: controllerTab,
        ));
  }
}
